# COVID-19 Global Forecasting
Starting in March 2020 Kaggle launched a series of challenges to predict the spread of COVID-19 across the globe. This repository contains our submission and various supplementary resources. 

## Overview
Between March and May 2020 Kaggle held a series of challenges to predict the cumulative  number of COVID-19 cases and fatalities in every country.  Predictions were to be made for each day in the month following the closing date of the challenge and for each country. We participated in [Week 4](https://www.kaggle.com/c/covid19-global-forecasting-week-4) for which the prediction period was from 16th April 2020 to 14th May 2020.

Our solution included data from a variety of novel sources, in particular:

- Country-level data such as GDP, population age distribution and broadband availability.
- Weather including temperature, rainfall and wind-speed.
- Government interventions such as the closure of schools, the closure of borders and a general lockdown.
- COVID-19 testing data for countries where this was being published.

Our model was built using `keras` on a `TensorFlow` backend. We tried a variety of model designs, however we quickly ran into overfitting problems which was unsurprising given the thin outcome period in the training data. We eventually settled on a auto-regressive graph-NN approach which made use of residual connections to help bolster the weight given to key inputs.

## Data
The data provided for model training was the daily count of cases and fatalities in each country compiled by John Hopkins University. However, the rules of the challenge encouraged the use of external datasets in order to derive useful insights and potentially establish causal relationships.

### Sources
Data was drawn from a variety of novel sources in an effort to establish an interesting insight into the vulnerability (or otherwise) of particular countries.

Dataset | Description 
-----------| ----------------
Country Info. | Cross-sectional country level information including various economic, demographic, health and geographic variables. Interesting examples include GDP, % of population over 60, and Physicians per 1000 population. 
Weather | Various weather related time-series over the train period. Examples include temperature, rainfall and wind speed.
Government Interventions | A compilation of actions taken by world governments to stem the spread of the pandemic in their respective countries. The dataset includes a description of the action, an approximate implementation date for the action and a categorisation of the action into higher-level categories such as social distancing orders, border closures, health measures, etc.
Testing | A count of the number of tests carried out as of a particular date. Only available for a small subset of countries and for a subset of dates.
Cases / Fatalities | The daily count of cases and fatalities in each country compiled by John Hopkins University. This was the basic train dataset provided with the challenge.

### Treatment
A suite of data cleaning and feature engineering procedures was carried out, but the exact sequence depends on the particular dataset. The table below documents which procedures were carried out on each dataset. Note that the order in which the procedures are applied are important, and the table should be read from left to right.

Dataset | Interpolation / Extrapolation | Growth rate calculation | Smoothing | Normalisation & Imputation
-|-|-|-|-
Country Info. | No | No | No | Yes
Weather | No | No | Yes | Yes
Government Interventions | Yes | No | No | No
Testing | Yes | Yes | Yes | Yes
Cases / Fatalities |  No | Yes | No | Yes

## Model
*This section is currently under construction*

### Outcome Period
One of the most difficult decisions during the model design and development stages was to choose the correct outcome period. The Kaggle challenge called for daily predictions over a one month period, so any outcome window between one day and one month would be feasible.  However, we found a tradeoff between the length of the outcome period and the accuracy of the model predictions. 

A natural choice for outcome period might be one month since that was the prediction period of the challenge. Given a long enough training window this may well have been an appropriate choice, however the training data that available to us included only the very start of the pandemic in many countries, and no reported cases in many others. Therefore, in many cases the model would frequently have been asked to predict a 1000% (or sometimes infinite) rise in cases and fatalities based on information given at a time when no cases at all. 

Conversely, if the outcome period was some period shorter than a month, say for example two weeks, then the model input would be more informative of the output it was expected to produce. However, by using an outcome period shorter than the total prediction period, we would have to find some scheme by which we could produce predictions beyond the first outcome window (in our example the first fortnight) in the test data. The key problem to overcome here is that the model is auto-regressive in nature, meaning that the output of the model (cases and fatalities in the future) is also an input to the model (cases and fatalities in the past), and so if we knew the value of that input in the future, there would be no need for a prediction at all.

In the final model we decided to use an outcome period of one week. The scheme we settled upon to solve the problem caused by auto-regression was to apply the model in a recursive manner, whereby the output of the model would be reused as an input into subsequent predictions of the model.

### NN Specification

Layer (type)                  |      Output Shape     |        Param #     | Connected to
--------|-|-|-
country| (InputLayer)               | [(None, 46)]             |0
measure (InputLayer)               | [(None, 39)]            | 0
dense_26 (Dense)                   | (None, 10)               |470          |country[0][0]
batch_normalization_4 (BatchNormali |(None, 39)            |   156          |measure[0][0]
weather (InputLayer)                |[(None, 1)]            |  0
geo (InputLayer)                    |[(None, 2)]              |0
dropout_14 (Dropout)                |(None, 10)             |  0            |dense_26[0][0]
dense_28 (Dense)                   | (None, 8)                |320          |batch_normalization_4[0][0]
dense_24 (Dense)                    |(None, 2)                |4           | weather[0][0]
dense_25 (Dense)                    |(None, 2)                |6            |geo[0][0]
dense_27 (Dense)                    |(None, 2)                |22           |dropout_14[0][0]
dropout_16 (Dropout)                |(None, 8)                |0            |dense_28[0][0]
covid_testing (InputLayer)          |[(None, 7)]              |0
dropout_12 (Dropout)                |(None, 2)                |0            |dense_24[0][0]
dropout_13 (Dropout)                |(None, 2)                |0            |dense_25[0][0]
dropout_15 (Dropout)                |(None, 2)                |0            |dense_27[0][0]
dense_29 (Dense)                    |(None, 2)                |18           |dropout_16[0][0]
dense_30 (Dense)                    |(None, 1)                |8            |covid_testing[0][0]
concatenate_4 (Concatenate)         |(None, 9)                |0           | dropout_12[0][0], dropout_13[0][0]  , dropout_15[0][0], dense_29[0][0], dense_30[0][0]
batch_normalization_5 (BatchNormali |(None, 9)                |36           |concatenate_4[0][0]
dense_32 (Dense)                    |(None, 8)                |80          | batch_normalization_5[0][0]
dropout_17 (Dropout)                |(None, 8)                |0            |dense_32[0][0]
lag (InputLayer)                   | [(None, 14)]             |0
dense_33 (Dense)                    |(None, 8)                |72           |dropout_17[0][0]
dense_31 (Dense)                    |(None, 2)                |30           |lag[0][0]
concatenate_5 (Concatenate)         |(None, 10)               |0            |dense_33[0][0], dense_31[0][0]
dense_34 (Dense)                    |(None, 4)                |44           |concatenate_5[0][0]
dense_35 (Dense)                    |(None, 2)                |10           |dense_34[0][0]

- Total params: 1,276
- Trainable params: 1,180
- Non-trainable params: 96

## Results

We produced predictions for the cumulative confirmed cases and fatalities in each country on each day between April 9th and May 14th. While at the time of submission the outcomes were of course unknown, with hindsight we have the ability to measure the performance of the model during this time period.

For a particular country on a particular day we define the prediction error to be the ratio between the model predicted count and actual count minus 1. In this metric a value of 0 indicates a perfect prediction. On each day of the prediction period we have a distribution of errors, since there is a prediction for each country/state on each date. 

The figures below show the median and interquartile range of the prediction error ratio over time. Sadly we see that the model underpredicts the spread of the pandemic, both in terms of cases and of fatalities. We also see that this prediction error compounds over time, which is not surprising given the recursive nature of the model application. By the end of the test window (May 14th) the median error was a 60% underprediction.

![](./results/plot_err_ratio_CC.png) 
![](./results/plot_err_ratio_F.png) 
